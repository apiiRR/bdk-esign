import 'package:flutter_dotenv/flutter_dotenv.dart';

class Endpoints {
  Endpoints._();

  static const String baseUrl = "https://apgdev.peruri.co.id:9044/gateway";
  static final String apiKey = dotenv.env['APIKEY']!;
  static final String systemId = dotenv.env['SYSTEMID']!;

  // receiveTimeout
  static const int receiveTimeout = 1000000;

  // connectTimeout
  static const int connectionTimeout = 1000000;

  //endpoints
  static const String getJson = "/jwtSandbox/1.0/getJsonWebToken/v1";
  static const String regis =
      "/digitalSignatureFullJwtSandbox/1.0/registration/v1";
  static const String videoVerif =
      "/digitalSignatureFullJwtSandbox/1.0/videoVerification/v1";
  static const String speciment =
      "/digitalSignatureFullJwtSandbox/1.0/sendSpeciment/v1";
  static const String checkCertif =
      "/digitalSignatureFullJwtSandbox/1.0/checkCertificate/v1";
}
