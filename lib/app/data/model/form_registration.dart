// To parse this JSON data, do
//
//     final formRegistration = formRegistrationFromJson(jsonString);

import 'dart:convert';

FormRegistration formRegistrationFromJson(String str) =>
    FormRegistration.fromJson(json.decode(str));

String formRegistrationToJson(FormRegistration data) =>
    json.encode(data.toJson());

class FormRegistration {
  FormRegistration({
    required this.name,
    required this.phone,
    required this.email,
    required this.password,
    required this.type,
    required this.ktp,
    required this.ktpPhoto,
    required this.npwp,
    required this.npwpPhoto,
    required this.selfPhoto,
    required this.address,
    required this.city,
    required this.province,
    required this.gender,
    required this.placeOfBirth,
    required this.dateOfBirth,
    required this.orgUnit,
    required this.workUnit,
    required this.position,
    required this.systemId,
  });

  String name;
  String phone;
  String email;
  String password;
  String type;
  String ktp;
  String ktpPhoto;
  String npwp;
  String npwpPhoto;
  String selfPhoto;
  String address;
  String city;
  String province;
  String gender;
  String placeOfBirth;
  String dateOfBirth;
  String orgUnit;
  String workUnit;
  String position;
  String systemId;

  factory FormRegistration.fromJson(Map<String, dynamic> json) =>
      FormRegistration(
        name: json["name"],
        phone: json["phone"],
        email: json["email"],
        password: json["password"],
        type: json["type"],
        ktp: json["ktp"],
        ktpPhoto: json["ktpPhoto"],
        npwp: json["npwp"],
        npwpPhoto: json["npwpPhoto"],
        selfPhoto: json["selfPhoto"],
        address: json["address"],
        city: json["city"],
        province: json["province"],
        gender: json["gender"],
        placeOfBirth: json["placeOfBirth"],
        dateOfBirth: json["dateOfBirth"],
        orgUnit: json["orgUnit"],
        workUnit: json["workUnit"],
        position: json["position"],
        systemId: json["systemId"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "phone": phone,
        "email": email,
        "password": password,
        "type": type,
        "ktp": ktp,
        "ktpPhoto": ktpPhoto,
        "npwp": npwp,
        "npwpPhoto": npwpPhoto,
        "selfPhoto": selfPhoto,
        "address": address,
        "city": city,
        "province": province,
        "gender": gender,
        "placeOfBirth": placeOfBirth,
        "dateOfBirth": dateOfBirth,
        "orgUnit": orgUnit,
        "workUnit": workUnit,
        "position": position,
        "systemId": systemId,
      };
}
