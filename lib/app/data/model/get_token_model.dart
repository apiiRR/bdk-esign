import 'dart:convert';

GetTokenModel getTokenModelFromJson(String str) => GetTokenModel.fromJson(json.decode(str));

String getTokenModelToJson(GetTokenModel data) => json.encode(data.toJson());

class GetTokenModel {
    GetTokenModel({
        required this.resultCode,
        required this.resultDesc,
        required this.data,
    });

    String resultCode;
    String resultDesc;
    Data data;

    factory GetTokenModel.fromJson(Map<String, dynamic> json) => GetTokenModel(
        resultCode: json["resultCode"],
        resultDesc: json["resultDesc"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "resultCode": resultCode,
        "resultDesc": resultDesc,
        "data": data.toJson(),
    };
}

class Data {
    Data({
        required this.jwt,
        required this.expiredDate,
    });

    String jwt;
    DateTime expiredDate;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        jwt: json["jwt"],
        expiredDate: DateTime.parse(json["expiredDate"]),
    );

    Map<String, dynamic> toJson() => {
        "jwt": jwt,
        "expiredDate": expiredDate.toIso8601String(),
    };
}