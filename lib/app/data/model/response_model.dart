// To parse this JSON data, do
//
//     final responseModel = responseModelFromJson(jsonString);

import 'dart:convert';

ResponseModel responseModelFromJson(String str) => ResponseModel.fromJson(json.decode(str));

String responseModelToJson(ResponseModel data) => json.encode(data.toJson());

class ResponseModel {
    String resultCode;
    String resultDesc;

    ResponseModel({
        required this.resultCode,
        required this.resultDesc,
    });

    factory ResponseModel.fromJson(Map<String, dynamic> json) => ResponseModel(
        resultCode: json["resultCode"],
        resultDesc: json["resultDesc"],
    );

    Map<String, dynamic> toJson() => {
        "resultCode": resultCode,
        "resultDesc": resultDesc,
    };
}
