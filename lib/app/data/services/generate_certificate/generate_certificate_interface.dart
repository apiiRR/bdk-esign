import 'package:dartz/dartz.dart';

import '../../model/response_model.dart';

abstract class GenerateCertificateInterface {
  Future<Either<ResponseModel, String>> postRegistration(data, String token);
  Future<Either<ResponseModel, String>> postVideoVerification(
      String email, String videoStream, String token);
  Future<Either<ResponseModel, String>> postSendSpesiment(
      String email, String speciment, String token);
  Future<Either<ResponseModel, String>> postCheckSpesiment(
      String email, String token);
  Future<bool> sendOtp(String email);
}
