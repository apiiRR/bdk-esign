import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';

import 'package:e_sign/app/data/model/response_model.dart';

import '../../dio_client.dart';
import '../../endpoints.dart';
import '../../exception.dart';
import 'generate_certificate_interface.dart';

class GenerateCertificateService implements GenerateCertificateInterface {
  final DioClient _client = DioClient();

  @override
  Future<Either<ResponseModel, String>> postCheckSpesiment(
      String email, token) async {
    try {
      final response = await _client.post(Endpoints.checkCertif,
          data: jsonEncode({
            "param": {"email": email, "systemId": Endpoints.systemId}
          }),
          options: Options(headers: {
            "Content-Type": "application/json",
            "x-Gateway-APIKey": Endpoints.apiKey,
            "Authorization": "Bearer $token"
          }));

      if (response.statusCode == 200) {
        ResponseModel data = ResponseModel.fromJson(response.data);
        return Left(data);
      } else {
        final String errorMessage =
            DioExceptions.fromDioError(response).errorMessage();
        return Right(errorMessage);
      }
    } on DioError catch (e) {
      final String errorMessage = DioExceptions.fromDioError(e).errorMessage();
      return Right(errorMessage);
    }
  }

  @override
  Future<Either<ResponseModel, String>> postSendSpesiment(
      String email, String speciment, String token) async {
    try {
      final response = await _client.post(Endpoints.getJson,
          data: jsonEncode({
            "param": {
              "systemId": Endpoints.systemId,
              "email": email,
              "speciment": speciment
            }
          }),
          options: Options(headers: {
            "Content-Type": "application/json",
            "x-Gateway-APIKey": Endpoints.apiKey,
            "Authorization": "Bearer $token"
          }));

      if (response.statusCode == 200) {
        ResponseModel data = ResponseModel.fromJson(response.data);
        return Left(data);
      } else {
        final String errorMessage =
            DioExceptions.fromDioError(response).errorMessage();
        return Right(errorMessage);
      }
    } on DioError catch (e) {
      final String errorMessage = DioExceptions.fromDioError(e).errorMessage();
      return Right(errorMessage);
    }
  }

  @override
  Future<Either<ResponseModel, String>> postVideoVerification(
      String email, String videoStream, String token) async {
    try {
      final response = await _client.post(Endpoints.videoVerif,
          data: jsonEncode({
            "param": {
              "systemId": Endpoints.systemId,
              "email": email,
              "videoStream": videoStream
            }
          }),
          options: Options(headers: {
            "x-Gateway-APIKey": Endpoints.apiKey,
            "Content-Type": "application/json",
            "Authorization": "Bearer $token"
          }));

      if (response.statusCode == 200) {
        ResponseModel data = ResponseModel.fromJson(response.data);
        return Left(data);
      } else {
        final String errorMessage =
            DioExceptions.fromDioError(response).errorMessage();
        return Right(errorMessage);
      }
    } on DioError catch (e) {
      print(e);
      final String errorMessage = DioExceptions.fromDioError(e).errorMessage();
      return Right(errorMessage);
    }
  }

  @override
  Future<Either<ResponseModel, String>> postRegistration(
      data, String token) async {
    try {
      final response = await _client.post(Endpoints.regis,
          data: jsonEncode({"param": data}),
          options: Options(headers: {
            "x-Gateway-APIKey": Endpoints.apiKey,
            "Content-Type": "application/json",
            "Authorization": "Bearer $token"
          }));

      if (response.statusCode == 200) {
        ResponseModel data = ResponseModel.fromJson(response.data);
        return Left(data);
      } else {
        final String errorMessage =
            DioExceptions.fromDioError(response).errorMessage();
        return Right(errorMessage);
      }
    } on DioError catch (e) {
      final String errorMessage = DioExceptions.fromDioError(e).errorMessage();
      return Right(errorMessage);
    }
  }

  @override
  Future<bool> sendOtp(String email) async {
    try {
      final Dio _dio = Dio();
      final response = await _dio
          .post("https://flutter.rohitchouhan.com/email-otpV2/v2.php", data: {
        "app_name": "Berdikari",
        "user_email": "michellarlndy30@gmail.com",
        "otp_length": 4,
        "type": "digits",
        "smtp_host": "smtp.gmail.com",
        "smtp_auth": true,
        "smtp_username": "it@ptberdikari.co.id",
        "smtp_password": "bigzfqibepxddmkv"
      });

      print(response.statusCode);
      print(response.data);
      return true;
      // final response = await _client.post(Endpoints.regis,
      //     data: jsonEncode({"param": data}),
      //     options: Options(headers: {
      //       "x-Gateway-APIKey": Endpoints.apiKey,
      //       "Content-Type": "application/json",
      //       "Authorization": "Bearer $token"
      //     }));

      // if (response.statusCode == 200) {
      //   ResponseModel data = ResponseModel.fromJson(response.data);
      //   return Left(data);
      // } else {
      //   final String errorMessage =
      //       DioExceptions.fromDioError(response).errorMessage();
      //   return Right(errorMessage);
      // }
    } on DioError catch (e) {
      // final String errorMessage = DioExceptions.fromDioError(e).errorMessage();
      // return Right(errorMessage);
      print(e);
      return false;
    }
  }
}
