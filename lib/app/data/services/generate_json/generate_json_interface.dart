import 'package:dartz/dartz.dart';

import '../../model/get_token_model.dart';

abstract class GenerateJsonInterface {
  Future<Either<GetTokenModel, String>> getToken();
}
