import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:e_sign/app/data/model/get_token_model.dart';

import '../../dio_client.dart';
import '../../endpoints.dart';
import '../../exception.dart';
import 'generate_json_interface.dart';

class GenerateJsonService implements GenerateJsonInterface {
  final DioClient _client = DioClient();

  @override
  Future<Either<GetTokenModel, String>> getToken() async {
    print("HIT TOKEN");
    try {
      final response = await _client.post(Endpoints.getJson,
          data: jsonEncode({
            "param": {"systemId": Endpoints.systemId}
          }),
          options: Options(headers: {
            "Content-Type": "application/json",
            "x-Gateway-APIKey": Endpoints.apiKey
          }));

      if (response.statusCode == 200) {
        GetTokenModel data = GetTokenModel.fromJson(response.data);
        return Left(data);
      } else {
        final String errorMessage =
            DioExceptions.fromDioError(response).errorMessage();
        return Right(errorMessage);
      }
    } on DioError catch (e) {
      final String errorMessage = DioExceptions.fromDioError(e).errorMessage();
      return Right(errorMessage);
    }
  }
}
