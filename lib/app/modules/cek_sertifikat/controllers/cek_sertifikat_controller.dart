import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../data/model/response_model.dart';
import '../../../data/services/generate_certificate/generate_certificate_service.dart';
import '../../utils/colors.dart';
import '../../utils/text_styles.dart';

class CekSertifikatController extends GetxController {
  var isLoading = false.obs;
  var isError = false.obs;
  var errmsg = "".obs;

  final String jwt = Get.arguments;
  ResponseModel? data;

  Future check(String email) async {
    isLoading(true);
    try {
      final result =
          await GenerateCertificateService().postCheckSpesiment(email, jwt);

      result.fold((l) {
        data = l;
      }, (r) {
        errmsg.value = r;
        isError(true);
      });

      isLoading(false);

      if (data == null) {
        isLoading(false);
        Get.snackbar("Kesalahan", errmsg.value);
        return;
      }

      switch (data!.resultCode) {
        case "0":
          Get.defaultDialog(
              title: "Informasi Sertifikat",
              content: Text(
                data!.resultDesc,
                style: text14(black, regular),
              ));
          break;
        case "08":
          Get.defaultDialog(
              title: "Informasi Sertifikat",
              content: Text(
                "Sertifikat sedang diproses..",
                style: text14(black, regular),
              ));
          break;
        case "02":
          Get.defaultDialog(
              title: "Informasi Sertifikat",
              content: Text(
                "Email belum didaftarkan.",
                style: text14(black, regular),
              ));
          break;
        case "04":
          Get.defaultDialog(
              title: "Informasi Sertifikat",
              content: Text(
                "Pendaftaran belum lengkap (Verifikasi wajah belum dilakukan)",
                style: text14(black, regular),
              ));
          break;
        default:
          Get.defaultDialog(
              title: "Informasi Sertifikat",
              titleStyle: text14(black, medium),
              content: Text(
                data!.resultDesc,
                style: text12(black, regular),
              ));
          break;
      }
    } catch (error) {
      isLoading(false);
      isError(true);
      errmsg(error.toString());
      throw Exception("Error $error");
    }
  }
}
