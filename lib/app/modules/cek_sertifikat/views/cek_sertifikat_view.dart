import 'package:e_sign/app/modules/utils/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

import 'package:get/get.dart';

import '../../utils/colors.dart';
import '../../widgets/input_item.dart';
import '../../widgets/loading_button.dart';
import '../../widgets/rounded_button.dart';
import '../controllers/cek_sertifikat_controller.dart';

class CekSertifikatView extends GetView<CekSertifikatController> {
  CekSertifikatView({Key? key}) : super(key: key);
  final _formKey = GlobalKey<FormBuilderState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryRed,
        title: Text(
          'Cek Sertifikat',
          style: text16(white, medium),
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: Get.width * 0.05),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FormBuilder(
                key: _formKey,
                child: Column(
                  children: [
                    SingleChildScrollView(
                      child: InputItem(
                        label: "Alamat Email",
                        name: "email",
                        hintText: "Masukkan alamat email",
                        validator: FormBuilderValidators.compose([
                          FormBuilderValidators.required(),
                          FormBuilderValidators.email()
                        ]),
                        keyboardType: TextInputType.emailAddress,
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Obx(() => controller.isLoading.isFalse
                        ? RoundedButton(
                            text: "Cek",
                            onTap: () {
                              _formKey.currentState!.save();
                              if (_formKey.currentState!.validate()) {
                                controller.check(
                                    _formKey.currentState!.value["email"]);
                              }
                            },
                          )
                        : const LoadingButton())
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
