import 'dart:convert';
import 'dart:io';

import 'package:e_sign/app/modules/utils/colors.dart';
import 'package:e_sign/app/modules/utils/text_styles.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';

import '../../../data/endpoints.dart';
import '../../../data/model/form_registration.dart';
import '../../../data/model/response_model.dart';
import '../../../data/services/generate_certificate/generate_certificate_service.dart';
import '../../../routes/app_pages.dart';
import '../../widgets/rounded_button.dart';
import '../views/components/input_box.dart';

class HomeController extends GetxController {
  var isLoading = false.obs;
  var isError = false.obs;
  var errmsg = "".obs;
  var aggrement = false.obs;
  ResponseModel? data;
  FirebaseAuth auth = FirebaseAuth.instance;
  final formkey = GlobalKey<FormState>();

  var currentPhone = "".obs;

  PageController controller = PageController();
  final String jwt = Get.arguments;
  PlatformFile? fileTandaTangan;
  PlatformFile? fotoKTP;
  PlatformFile? fotoNPWP;
  PlatformFile? fotoPegawai;

  String name = "";
  String phone = "";
  String email = "";
  String password = "";
  String ktp = "";
  final String type = "INDIVIDUAL";
  String ktpPhoto = "";
  String npwp = "";
  String npwpPhoto = "";
  String address = "";
  String city = "";
  String province = "";
  String gender = "";
  String placeOfBirth = "";
  String dateOfBirth = "";
  String orgUnit = "";
  String workUnit = "PT Berdikari";
  String position = "";
  String signature = "";
  String selfPhoto = "";

  var pin1 = "".obs;
  var pin2 = "".obs;
  var pin3 = "".obs;
  var pin4 = "".obs;
  var pin5 = "".obs;
  var pin6 = "".obs;

  var index = 0.obs;
  var phoneVerified = false.obs;
  var loadingVerified = false.obs;

  void changeIndex() {
    index++;
  }

  void changeIndexBefore() {
    index--;
  }

  void pickFiles(int index) async {
    FilePickerResult? result;
    result = await FilePicker.platform.pickFiles(type: FileType.image);

    if (result != null) {
      PlatformFile fileTemp = result.files.first;
      if (fileTemp.size >= 1000000) {
        Get.snackbar(
            "Kesalahan", "File yang diunggah tidak boleh lebih dari 1 Mb");
        return;
      }
      File imagefile = File(fileTemp.path!);
      Uint8List imagebytes = await imagefile.readAsBytes();
      String base64string = base64.encode(imagebytes);
      switch (index) {
        case 1:
          fileTandaTangan = result.files.first;
          signature = base64string;
          break;
        case 2:
          fotoKTP = result.files.first;
          ktpPhoto = base64string;
          break;
        case 3:
          fotoNPWP = result.files.first;
          npwpPhoto = base64string;
          break;
        case 4:
          fotoPegawai = result.files.first;
          selfPhoto = base64string;
          break;
        default:
      }
      // PlatformFile file = result.files.first;

      // print(file.name);
      // print(file.bytes);
      // print(file.size);
      // print(file.extension);
      // print(file.path);
    }

    update();
  }

  void verifyPhone(String phone) async {
    await auth.verifyPhoneNumber(
      phoneNumber: '+62 $phone',
      verificationCompleted: (PhoneAuthCredential credential) {},
      verificationFailed: (FirebaseAuthException e) {
        Get.snackbar("Kesalahan", e.message!);
      },
      codeSent: (String verificationId, int? resendToken) async {
        await Get.defaultDialog(
            title: "Verifikasi OTP",
            titleStyle: text16(black, regular),
            // barrierDismissible: false,
            content: Form(
                key: formkey,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InputBox(
                          onSaved: (pin) {
                            pin1.value = pin!;
                          },
                          isFirst: true,
                        ),
                        InputBox(
                          onSaved: (pin) {
                            pin2.value = pin!;
                          },
                        ),
                        InputBox(
                          onSaved: (pin) {
                            pin3.value = pin!;
                          },
                        ),
                        InputBox(
                          onSaved: (pin) {
                            pin4.value = pin!;
                          },
                        ),
                        InputBox(
                          onSaved: (pin) {
                            pin5.value = pin!;
                          },
                        ),
                        InputBox(
                          onSaved: (pin) {
                            pin6.value = pin!;
                          },
                        ),
                      ],
                    ),
                    // SizedBox(
                    //   height: Get.height * 0.02,
                    // ),
                    // Row(
                    //   children: [
                    //     Text(
                    //       "Don’t receive a code?",
                    //       style: text12(const Color(0xFF9A9A9A), regular),
                    //     ),
                    //     GestureDetector(
                    //       onTap: () {
                    //         controller.sendOtp();
                    //       },
                    //       child: Container(
                    //         margin: const EdgeInsets.only(left: 5),
                    //         child: Text(
                    //           "Resend",
                    //           style: text12(primaryRed, regular),
                    //         ),
                    //       ),
                    //     )
                    //   ],
                    // ),
                    SizedBox(
                      height: Get.height * 0.05,
                    ),
                    RoundedButton(
                      text: "Konfirmasi",
                      onTap: () async {
                        if (formkey.currentState!.validate()) {
                          formkey.currentState!.save();
                          if (pin1.value != "" &&
                              pin2.value != "" &&
                              pin3.value != "" &&
                              pin4.value != "" &&
                              pin5.value != "" &&
                              pin6.value != "") {
                            PhoneAuthCredential credential =
                                PhoneAuthProvider.credential(
                              verificationId: verificationId,
                              smsCode: "$pin1$pin2$pin3$pin4$pin5$pin6",
                            );
                            await auth
                                .signInWithCredential(credential)
                                .then((value) {
                              currentPhone.value = phone;
                              phoneVerified(true);
                              Get.back();
                              auth.signOut();
                            });
                          } else {
                            Get.snackbar("Perhatian", "Kode OTP tidak lengkap");
                          }
                        }
                      },
                    )
                  ],
                )));
      },
      codeAutoRetrievalTimeout: (String verificationId) {},
    );
  }

  void inputStepOne(
      name, email, password, phone, gender, ktp, npwp, ou, posisi) {
    this.name = name;
    this.email = email;
    this.password = password;
    this.phone = phone;
    this.gender = gender == "Laki-Laki" ? "M" : "F";
    this.ktp = ktp;
    this.npwp = npwp ?? "";
    orgUnit = ou ?? "";
    position = posisi ?? "";
    update();
  }

  void inputStepTwo(tempatLahir, tanggalLahir, provinsi, kota, alamat) {
    final years = Jiffy.parse(DateFormat('EEE, M/d/y').format(tanggalLahir),
            pattern: "EEE, M/d/y")
        .fromNow();
    final yearsInt = int.tryParse("${years[0]}${years[1]}");
    if (yearsInt! < 17) {
      Get.snackbar("Kesalahan", "Minimum age is 17 years");
    } else {
      placeOfBirth = tempatLahir;
      dateOfBirth = tanggalLahir.toString();
      province = provinsi;
      city = kota;
      address = alamat;
      update();

      changeIndex();
      controller.animateToPage(index.value,
          duration: const Duration(milliseconds: 500), curve: Curves.easeIn);
    }
  }

  Future sendData() async {
    print(jwt);
    isLoading(true);
    try {
      final regis = await GenerateCertificateService().postRegistration(
        FormRegistration(
            name: name,
            phone: phone,
            email: email,
            password: password,
            type: type,
            ktp: ktp,
            ktpPhoto: ktpPhoto,
            npwp: npwp,
            npwpPhoto: npwpPhoto,
            selfPhoto: selfPhoto,
            address: address,
            city: city,
            province: province,
            gender: gender,
            placeOfBirth: placeOfBirth,
            dateOfBirth: dateOfBirth,
            orgUnit: orgUnit,
            workUnit: workUnit,
            position: position,
            systemId: Endpoints.systemId),
        jwt,
      );

      regis.fold((l) {
        data = l;
      }, (r) {
        errmsg.value = r;
        isError(true);
      });

      if (data == null) {
        isLoading(false);
        Get.snackbar("Kesalahan", errmsg.value);
        return;
      }

      if (data != null && data!.resultCode != "0") {
        isLoading(false);
        Get.snackbar("Kesalahan", data!.resultDesc);
        return;
      }

      final speciment = await GenerateCertificateService()
          .postSendSpesiment(email, signature, jwt);

      speciment.fold((l) {
        data = l;
      }, (r) {
        errmsg.value = r;
        isError(true);
      });

      if (data == null) {
        isLoading(false);
        Get.snackbar("Kesalahan", errmsg.value);
        return;
      }

      if (data != null && data!.resultCode != "0") {
        isLoading(false);
        Get.snackbar("Kesalahan", data!.resultDesc);
        return;
      }

      isLoading(false);
      Get.toNamed(Routes.OTP, arguments: [email, jwt]);
      // Get.offAllNamed(Routes.VIDEO_VERIFICATION,
      //     arguments: {"jwt": jwt, "email": email});
    } catch (error) {
      isLoading(false);
      isError(true);
      errmsg(error.toString());
      throw Exception("Error $error");
    }
  }
}
