import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../utils/colors.dart';

class InputBox extends StatelessWidget {
  const InputBox({Key? key, required this.onSaved, this.isFirst = false})
      : super(key: key);

  final void Function(String?) onSaved;
  final bool isFirst;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: 30,
      decoration: BoxDecoration(
        border: Border.all(color: primaryRed),
      ),
      child: Center(
        child: TextFormField(
          autofocus: isFirst ? true : false,
          cursorColor: primaryRed,
          decoration: const InputDecoration(border: InputBorder.none),
          onChanged: (value) {
            if (value.length == 1) {
              FocusScope.of(context).nextFocus();
            }
          },
          onSaved: onSaved,
          textAlign: TextAlign.center,
          keyboardType: TextInputType.number,
          inputFormatters: [
            LengthLimitingTextInputFormatter(1),
            FilteringTextInputFormatter.digitsOnly
          ],
        ),
      ),
    );
  }
}
