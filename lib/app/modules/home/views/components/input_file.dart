import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../utils/colors.dart';
import '../../../utils/text_styles.dart';

class InputFile extends StatelessWidget {
  const InputFile(
      {super.key,
      required this.label,
      required this.onTap,
      this.info,
      this.fileName});

  final String label;
  final String? info;
  final void Function()? onTap;
  final String? fileName;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Wrap(
          children: [
            Text(
              label,
              style: text12(black, medium),
            ),
            if (info != null) ...[
              Text(
                info!,
                style: const TextStyle(
                    color: primaryRed,
                    fontSize: 12,
                    fontWeight: regular,
                    fontStyle: FontStyle.italic),
              ),
            ]
          ],
        ),
        const SizedBox(
          height: 6,
        ),
        Container(
            width: Get.width,
            padding: const EdgeInsets.all(12),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(8)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: Get.width,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: white,
                          shape: RoundedRectangleBorder(
                              side:
                                  const BorderSide(color: primaryRed, width: 2),
                              borderRadius: BorderRadius.circular(8))),
                      onPressed: onTap,
                      child: Text(
                        "Upload File",
                        style: text12(primaryRed, medium),
                      )),
                ),
                if (fileName != null)
                  Text(
                    fileName!,
                    style: text12(black, regular),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                  )
              ],
            )),
      ],
    );
  }
}
