import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';

import '../../../utils/colors.dart';
import '../../../utils/text_styles.dart';
import '../../../widgets/dropdown_item.dart';
import '../../../widgets/input_item.dart';
import '../../../widgets/rounded_button.dart';
import '../../controllers/home_controller.dart';

class StepOne extends StatelessWidget {
  StepOne({
    super.key,
  });

  final _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    List<String> genderOptions = ["Laki-Laki", "Perempuan"];
    return SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: Get.width * 0.05),
        child: GetBuilder<HomeController>(builder: (controller) {
          return FormBuilder(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 16,
                ),
                InputItem(
                  label: "Nama Lengkap*",
                  name: "nama_lengkap",
                  hintText: "Masukkan nama lengkap",
                  info: "(Sesuai KTP)",
                  validator: FormBuilderValidators.required(),
                  keyboardType: TextInputType.name,
                  initialValue: controller.name,
                ),
                const SizedBox(
                  height: 10,
                ),
                InputItem(
                  label: "Alamat Email*",
                  name: "email",
                  hintText: "Masukkan alamat email",
                  info: "(Harus Email Perusahaan)",
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(),
                    FormBuilderValidators.email()
                  ]),
                  keyboardType: TextInputType.emailAddress,
                  initialValue: controller.email,
                ),
                const SizedBox(
                  height: 10,
                ),
                InputItem(
                  label: "Password*",
                  name: "password",
                  hintText: "Masukkan password akun",
                  validator: FormBuilderValidators.required(),
                  isPassword: true,
                  keyboardType: TextInputType.visiblePassword,
                  initialValue: controller.password,
                ),
                const SizedBox(
                  height: 10,
                ),
                InputItem(
                  label: "Ulangi Password*",
                  name: "password_ulangi",
                  hintText: "Ulangi password akun",
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(),
                  ]),
                  isPassword: true,
                  keyboardType: TextInputType.visiblePassword,
                  initialValue: controller.password,
                ),
                const SizedBox(
                  height: 10,
                ),
                InputItem(
                  label: "Nomor Handphone*",
                  name: "nomor_handphone",
                  hintText: "Masukkan nomor handphone",
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(),
                    FormBuilderValidators.numeric()
                  ]),
                  keyboardType: TextInputType.phone,
                  initialValue: controller.phone,
                  onChanged: (String? value) {
                    if (value != null) {
                      if (value != controller.currentPhone.value) {
                        controller.phoneVerified(false);
                      }
                    }
                  },
                ),
                Obx(
                  () => Row(
                    children: [
                      if (controller.phoneVerified.isFalse) ...[
                        Text(
                          "Nomor anda belum terverifikasi ",
                          style: text12(black, regular),
                        ),
                      ],
                      controller.loadingVerified.isTrue
                          ? const SizedBox(
                              width: 10,
                              height: 10,
                              child: CircularProgressIndicator(
                                color: primaryRed,
                              ))
                          : Obx(() => controller.phoneVerified.isFalse
                              ? GestureDetector(
                                  onTap: () {
                                    controller.loadingVerified(true);
                                    _formKey.currentState!.save();
                                    if (_formKey.currentState!
                                                .value["nomor_handphone"] !=
                                            null &&
                                        _formKey.currentState!
                                                .value["nomor_handphone"] !=
                                            "") {
                                      controller.verifyPhone(_formKey
                                          .currentState!
                                          .value["nomor_handphone"]);
                                      controller.loadingVerified(false);
                                    } else {
                                      controller.loadingVerified(false);
                                      Get.snackbar("Kesalahan",
                                          "Nomor handphone masih kosong");
                                    }
                                  },
                                  child: Text(
                                    "Verifikasi Sekarang",
                                    style: text12(primaryRed, regular).copyWith(
                                        decoration: TextDecoration.underline),
                                  ),
                                )
                              : Text(
                                  "Terverifikasi",
                                  style: text12(Colors.green, regular),
                                ))
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                DropdownItem(
                  label: "Jenis Kelamin*",
                  name: "jenis_kelamin",
                  hintText: "Pilih jenis kelamin",
                  validator: FormBuilderValidators.required(),
                  items: genderOptions
                      .map((gender) => DropdownMenuItem(
                            value: gender,
                            child: Text(gender, style: text14(black, regular)),
                          ))
                      .toList(),
                  initialValue: controller.gender != ""
                      ? (controller.gender == "M" ? "Laki-Laki" : "Perempuan")
                      : null,
                ),
                const SizedBox(
                  height: 10,
                ),
                InputItem(
                  label: "Nomor KTP*",
                  name: "nik",
                  hintText: "Masukkan NIK",
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(),
                    FormBuilderValidators.numeric(),
                    FormBuilderValidators.minLength(13),
                    FormBuilderValidators.maxLength(16)
                  ]),
                  keyboardType: TextInputType.number,
                  initialValue: controller.ktp,
                ),
                const SizedBox(
                  height: 10,
                ),
                InputItem(
                  label: "Nomor NPWP",
                  name: "npwp",
                  hintText: "Masukkan nomor NPWP",
                  info: "(optional)",
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.numeric(),
                    FormBuilderValidators.maxLength(15)
                  ]),
                  keyboardType: TextInputType.number,
                  initialValue: controller.npwp,
                ),
                const SizedBox(
                  height: 10,
                ),
                InputItem(
                  label: "Nama Unit Organisasi (OU)",
                  name: "ou",
                  hintText: "Masukkan nama unit organisasi",
                  validator: null,
                  keyboardType: TextInputType.text,
                  initialValue: controller.orgUnit,
                ),
                const SizedBox(
                  height: 10,
                ),
                InputItem(
                  label: "Jabatan Kerja",
                  name: "jabatan",
                  hintText: "Masukkan jabatan kerja",
                  validator: null,
                  keyboardType: TextInputType.text,
                  initialValue: controller.position,
                ),
                const SizedBox(
                  height: 32,
                ),
                RoundedButton(
                  text: "Selanjutnya",
                  onTap: () {
                    _formKey.currentState!.save();
                    if (_formKey.currentState!.validate()) {
                      if (_formKey.currentState!.value["password"] ==
                          _formKey.currentState!.value["password_ulangi"]) {
                        if (controller.phoneVerified.isTrue) {
                          controller.inputStepOne(
                              _formKey.currentState!.value["nama_lengkap"],
                              _formKey.currentState!.value["email"],
                              _formKey.currentState!.value["password"],
                              _formKey.currentState!.value["nomor_handphone"],
                              _formKey.currentState!.value["jenis_kelamin"],
                              _formKey.currentState!.value["nik"],
                              _formKey.currentState!.value["npwp"],
                              _formKey.currentState!.value["ou"],
                              _formKey.currentState!.value["jabatan"]);
                          controller.changeIndex();
                          controller.controller.animateToPage(
                              controller.index.value,
                              duration: const Duration(milliseconds: 500),
                              curve: Curves.easeIn);
                        } else {
                          Get.snackbar(
                              "Error", "Nomor handphone belum terverifikasi",
                              backgroundColor: white);
                        }
                      } else {
                        Get.snackbar(
                            "Error", "the passwords entered are not the same",
                            backgroundColor: white);
                      }
                    }
                  },
                ),
                const SizedBox(
                  height: 30,
                ),
              ],
            ),
          );
        }));
  }
}
