import 'package:e_sign/app/modules/widgets/loading_button.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../utils/colors.dart';
import '../../../utils/text_styles.dart';
import '../../../widgets/rounded_button.dart';
import '../../../widgets/rounded_button_unfill.dart';
import '../../controllers/home_controller.dart';
import 'input_file.dart';

class StepThree extends StatelessWidget {
  const StepThree({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: Get.width * 0.05),
        child: GetBuilder<HomeController>(
          builder: (controller) => Column(
            children: [
              const SizedBox(
                height: 16,
              ),
              InputFile(
                onTap: () {
                  controller.pickFiles(1);
                },
                label: "Gambar Tanda Tangan",
                info: "(optional)(Max. File 1 Mb)",
                fileName: controller.fileTandaTangan != null
                    ? controller.fileTandaTangan!.name
                    : null,
              ),
              const SizedBox(
                height: 10,
              ),
              InputFile(
                onTap: () {
                  controller.pickFiles(2);
                },
                label: "Foto KTP*",
                info: "(Max. File 1 Mb)",
                fileName: controller.fotoKTP != null
                    ? controller.fotoKTP!.name
                    : null,
              ),
              const SizedBox(
                height: 10,
              ),
              InputFile(
                onTap: () {
                  controller.pickFiles(3);
                },
                label: "Foto NPWP",
                info: "(optional)(Max. File 1 Mb)",
                fileName: controller.fotoNPWP != null
                    ? controller.fotoNPWP!.name
                    : null,
              ),
              const SizedBox(
                height: 10,
              ),
              InputFile(
                onTap: () {
                  controller.pickFiles(4);
                },
                label: "Foto Kartu Pegawai Tampak Depan*",
                info: "(Max. File 1 Mb)",
                fileName: controller.fotoPegawai != null
                    ? controller.fotoPegawai!.name
                    : null,
              ),
              const SizedBox(
                height: 32,
              ),
              Obx(() => Row(
                    children: [
                      Checkbox(
                        value: controller.aggrement.value,
                        onChanged: (condition) {
                          controller.aggrement(condition);
                        },
                        checkColor: white,
                        activeColor: primaryRed,
                      ),
                      Expanded(
                        child: RichText(
                            text: TextSpan(children: [
                          TextSpan(
                              style: text12(black, regular),
                              text: "Saya setuju dengan"),
                          TextSpan(
                              style: text12(primaryBlue, semibold),
                              text: " Syarat dan Ketentuan Legal ",
                              recognizer: TapGestureRecognizer()
                                ..onTap = () async {
                                  final Uri url = Uri.parse(
                                      'https://ca.peruri.co.id/ca/legal/');
                                  if (!await launchUrl(url,
                                      mode: LaunchMode.externalApplication)) {
                                    throw Exception('Could not launch $url');
                                  }
                                }),
                          TextSpan(
                              style: text12(black, regular), text: "Peruri CA"),
                        ])),
                      )
                    ],
                  )),
              const SizedBox(
                height: 10,
              ),
              Obx(() => controller.isLoading.isFalse
                  ? RoundedButton(
                      text: "Registrasi",
                      onTap: () {
                        if (controller.fotoKTP == null &&
                            controller.fotoPegawai == null) {
                          Get.snackbar("Kesalahan",
                              "Foto KTP dan Foto Kartu Pegawai wajib diisi",
                              duration: const Duration(milliseconds: 1000));
                        } else if (controller.aggrement.isFalse) {
                          Get.snackbar("Kesalahan",
                              "Anda belum menyetujui syarat dan ketentuan legal peruri",
                              duration: const Duration(milliseconds: 1000));
                        } else {
                          controller.sendData();
                        }
                      })
                  : const LoadingButton()),
              const SizedBox(
                height: 8,
              ),
              RoundedButtonUnfill(
                text: "Sebelumnya",
                onTap: () {
                  controller.changeIndexBefore();
                  controller.controller.animateToPage(controller.index.value,
                      duration: const Duration(milliseconds: 500),
                      curve: Curves.easeIn);
                },
              ),
              const SizedBox(
                height: 10,
              )
            ],
          ),
        ));
  }
}
