import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';

import '../../../widgets/date_picker_item.dart';
import '../../../widgets/input_item.dart';
import '../../../widgets/rounded_button.dart';
import '../../../widgets/rounded_button_unfill.dart';
import '../../controllers/home_controller.dart';

class StepTwo extends StatelessWidget {
  StepTwo({
    super.key,
  });
  final _formKeyTwo = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: Get.width * 0.05),
        child: GetBuilder<HomeController>(builder: (controller) {
          return FormBuilder(
            key: _formKeyTwo,
            child: Column(
              children: [
                const SizedBox(
                  height: 16,
                ),
                InputItem(
                  label: "Tempat Lahir*",
                  name: "tempat_lahir",
                  hintText: "Masukkan tempat lahir",
                  validator: FormBuilderValidators.required(),
                  keyboardType: TextInputType.text,
                  initialValue: controller.placeOfBirth,
                ),
                const SizedBox(
                  height: 10,
                ),
                DatePickerItem(
                    label: "Tanggal Lahir*",
                    name: "tanggal_lahir",
                    hintText: "Format: dd/mm/yyyy",
                    // validator: (value) {
                    //   if (value != null) {
                    //     final years = Jiffy.parse(
                    //             DateFormat('EEE, M/d/y').format(value),
                    //             pattern: "EEE, M/d/y")
                    //         .fromNow();
                    //     final yearsInt = int.tryParse("${years[0]}${years[1]}");
                    //     if (yearsInt! < 17) {
                    //       return "Minimum age is 17 years";
                    //     }
                    //   }
                    //   return null;
                    // },
                    validator: FormBuilderValidators.required(),
                    initialValue: controller.dateOfBirth != ""
                        ? DateTime.parse(controller.dateOfBirth)
                        : null),
                const SizedBox(
                  height: 10,
                ),
                InputItem(
                  label: "Provinsi*",
                  name: "provinsi",
                  hintText: "Masukkan provinsi",
                  validator: FormBuilderValidators.required(),
                  keyboardType: TextInputType.text,
                  initialValue: controller.province,
                ),
                const SizedBox(
                  height: 10,
                ),
                InputItem(
                  label: "Kota*",
                  name: "kota",
                  hintText: "Masukkan kota tempat tinggal",
                  validator: FormBuilderValidators.required(),
                  keyboardType: TextInputType.text,
                  initialValue: controller.city,
                ),
                const SizedBox(
                  height: 10,
                ),
                InputItem(
                  label: "Alamat Rumah*",
                  name: "alamat",
                  hintText: "Masukkan alamat rumah",
                  maxLines: 3,
                  validator: FormBuilderValidators.required(),
                  keyboardType: TextInputType.streetAddress,
                  initialValue: controller.address,
                ),
                const SizedBox(
                  height: 32,
                ),
                RoundedButton(
                  text: "Selanjutnya",
                  onTap: () {
                    _formKeyTwo.currentState!.save();
                    if (_formKeyTwo.currentState!.validate()) {
                      controller.inputStepTwo(
                        _formKeyTwo.currentState!.value["tempat_lahir"],
                        _formKeyTwo.currentState!.value["tanggal_lahir"],
                        _formKeyTwo.currentState!.value["provinsi"],
                        _formKeyTwo.currentState!.value["kota"],
                        _formKeyTwo.currentState!.value["alamat"],
                      );
                    }
                  },
                ),
                const SizedBox(
                  height: 8,
                ),
                RoundedButtonUnfill(
                  text: "Sebelumnya",
                  onTap: () {
                    controller.changeIndexBefore();
                    controller.controller.animateToPage(controller.index.value,
                        duration: const Duration(milliseconds: 500),
                        curve: Curves.easeIn);
                  },
                )
              ],
            ),
          );
        }));
  }
}
