import 'package:e_sign/app/modules/utils/colors.dart';
import 'package:e_sign/app/modules/utils/text_styles.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/home_controller.dart';
import 'components/step_one.dart';
import 'components/step_three.dart';
import 'components/step_two.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: primaryRed,
          title: Text(
            'Registrasi',
            style: text16(white, medium),
          ),
          centerTitle: true,
        ),
        body: PageView(
          controller: controller.controller,
          physics: const NeverScrollableScrollPhysics(),
          children: [StepOne(), StepTwo(), const StepThree()],
        ));
  }
}
