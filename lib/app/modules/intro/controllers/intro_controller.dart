import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';

import '../../../data/services/generate_json/generate_json_service.dart';
import '../../../data/model/get_token_model.dart';

class IntroController extends GetxController {
  String? jwt;
  var isLoading = false.obs;
  var isError = false.obs;
  var errmsg = "".obs;
  GetTokenModel? data;

  FirebaseFirestore firestore = FirebaseFirestore.instance;
  CollectionReference jwtData = FirebaseFirestore.instance.collection('jwt');

  DateTime now = DateTime.now();

  Future getToken() async {
    DateTime dateNow = DateTime(now.year, now.month, now.day);
    jwt = null;
    isLoading(true);
    try {
      final Map<String, dynamic> jwtFire = await getJwt();

      if (jwtFire["jwt"] != "") {
        if (DateTime.parse(jwtFire["date"]) != dateNow) {
          final result = await GenerateJsonService().getToken();

          result.fold((l) {
            data = l;
          }, (r) {
            errmsg.value = r;
            isError(true);
          });

          if (data != null && data!.resultCode == "0") {
            jwt = data!.data.jwt;
            await setJwt(jwt!, dateNow.toIso8601String());
          }
        } else {
          jwt = jwtFire["jwt"];
        }
      } else {
        final result = await GenerateJsonService().getToken();

        result.fold((l) {
          data = l;
        }, (r) {
          errmsg.value = r;
          isError(true);
        });

        if (data != null && data!.resultCode == "0") {
          jwt = data!.data.jwt;

          await setJwt(jwt!, dateNow.toIso8601String());
        }
      }

      isLoading(false);
    } catch (error) {
      isLoading(false);
      isError(true);
      errmsg(error.toString());
      throw Exception("Error $error");
    }
  }

  Future<Map<String, dynamic>> getJwt() async {
    DocumentSnapshot data = await jwtData.doc("berdikariJwt").get();
    Map<String, dynamic> dataMap = data.data() as Map<String, dynamic>;
    return dataMap;
  }

  Future setJwt(String jwt, String date) async {
    await jwtData.doc('berdikariJwt').update({'jwt': jwt, 'date': date});
  }
}
