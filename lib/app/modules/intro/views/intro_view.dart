import 'package:e_sign/app/modules/utils/text_styles.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../routes/app_pages.dart';
import '../../utils/colors.dart';
import '../../widgets/rounded_button.dart';
import '../../widgets/rounded_button_unfill.dart';
import '../controllers/intro_controller.dart';

class IntroView extends GetView<IntroController> {
  const IntroView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: Get.width * 0.05),
        child: Column(
          children: [
            SizedBox(
              height: Get.height / 2,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Digital",
                        style: text36(Colors.black, bold),
                      ),
                      Text(
                        "Document",
                        style: text36(Colors.black, regular),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Obx(() => !controller.isLoading.isFalse
                ? const Flexible(
                    child: CircularProgressIndicator(
                      color: primaryRed,
                    ),
                  )
                : Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        RoundedButton(
                          text: "Registrasi",
                          onTap: () async {
                            await controller.getToken();
                            if (controller.jwt != null) {
                              print(controller.jwt);
                              Get.offAllNamed(Routes.HOME,
                                  arguments: controller.jwt);
                            } else {
                              Get.snackbar(
                                  "Kesalahan", controller.errmsg.value);
                            }
                          },
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        RoundedButtonUnfill(
                          text: "Cek Sertikat",
                          onTap: () async {
                            await controller.getToken();
                            if (controller.jwt != null) {
                              Get.toNamed(Routes.CEK_SERTIFIKAT,
                                  arguments: controller.jwt);
                            } else {
                              Get.snackbar(
                                  "Kesalahan", controller.errmsg.value);
                            }
                          },
                        )
                      ],
                    ),
                  )),
            const SizedBox(
              height: 20,
            )
          ],
        ),
      ),
    );
  }
}
