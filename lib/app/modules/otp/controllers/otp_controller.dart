import 'package:email_otp/email_otp.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../routes/app_pages.dart';

class OtpController extends GetxController {
  final formkey = GlobalKey<FormState>();
  final String email = Get.arguments[0];
  final String jwt = Get.arguments[1];
  EmailOTP myauth = EmailOTP();
  var pin1 = "".obs;
  var pin2 = "".obs;
  var pin3 = "".obs;
  var pin4 = "".obs;

  void initOtp() async {
    myauth.setConfig(
        appName: "Berdikari E-Sign",
        appEmail: "it@ptberdikari.co.id",
        userEmail: email,
        otpLength: 4,
        otpType: OTPType.digitsOnly);

    myauth.setSMTP(
      host: "smtp.gmail.com",
      username: "it@ptberdikari.co.id",
      password: "bigzfqibepxddmkv",
    );
  }

  void sendOtp() async {
    final otp = await myauth.sendOTP();
    if (otp == true) {
      Get.snackbar("Berhasil", "Kode OTP berhasil dikirim ke $email");
    } else {
      Get.snackbar("Gagal",
          "Kode OTP tidak berhasil dikirim ke $email, silahkan cek email anda kembali");
    }
  }

  void verifyOtp() async {
    final otp = "$pin1$pin2$pin3$pin4";
    final verify = await myauth.verifyOTP(otp: otp);
    if (verify == true) {
      Get.snackbar("Berhasil", "Kode OTP berhasil diverifikasi");
      Get.offAllNamed(Routes.VIDEO_VERIFICATION,
          arguments: {"jwt": jwt, "email": email});
    } else {
      Get.snackbar("Gagal",
          "Kode OTP tidak berhasil diverifikasi, pastikan email anda benar");
    }
  }

  @override
  void onReady() {
    initOtp();
    sendOtp();
    super.onReady();
  }
}
