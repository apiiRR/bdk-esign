import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../utils/colors.dart';

class InputBox extends StatelessWidget {
  const InputBox({Key? key, required this.onSaved}) : super(key: key);

  final void Function(String?) onSaved;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      width: 60,
      decoration: const BoxDecoration(
        border: Border(bottom: BorderSide(color: primaryRed, width: 3)),
      ),
      child: Center(
        child: TextFormField(
          cursorColor: primaryRed,
          decoration: const InputDecoration(border: InputBorder.none),
          onChanged: (value) {
            if (value.length == 1) {
              FocusScope.of(context).nextFocus();
              
            }
          },
          onSaved: onSaved,
          textAlign: TextAlign.center,
          keyboardType: TextInputType.number,
          inputFormatters: [
            LengthLimitingTextInputFormatter(1),
            FilteringTextInputFormatter.digitsOnly
          ],
        ),
      ),
    );
  }
}
