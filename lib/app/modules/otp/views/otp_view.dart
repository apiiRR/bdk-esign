import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../utils/colors.dart';
import '../../utils/text_styles.dart';
import '../../widgets/rounded_button.dart';
import '../controllers/otp_controller.dart';
import 'components/input_box.dart';

class OtpView extends GetView<OtpController> {
  const OtpView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryRed,
        title: Text(
          'Verifikasi Email',
          style: text16(white, medium),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: Get.width * 0.05),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: Get.height * 0.03,
              ),
              Text(
                "Kode Verifikasi",
                style: text16(black, semibold),
              ),
              const SizedBox(height: 4),
              Text(
                "Kami sudah mengirimkan kode verifikasi ke email ${controller.email}",
                style: text14(black, regular),
              ),
              const SizedBox(height: 30),
              Form(
                  key: controller.formkey,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InputBox(
                            onSaved: (pin1) {
                              controller.pin1.value = pin1!;
                            },
                          ),
                          InputBox(
                            onSaved: (pin2) {
                              controller.pin2.value = pin2!;
                            },
                          ),
                          InputBox(
                            onSaved: (pin3) {
                              controller.pin3.value = pin3!;
                            },
                          ),
                          InputBox(
                            onSaved: (pin4) {
                              controller.pin4.value = pin4!;
                            },
                          ),
                        ],
                      ),
                      SizedBox(
                        height: Get.height * 0.02,
                      ),
                      Row(
                        children: [
                          Text(
                            "Don’t receive a code?",
                            style: text12(const Color(0xFF9A9A9A), regular),
                          ),
                          GestureDetector(
                            onTap: () {
                              controller.sendOtp();
                            },
                            child: Container(
                              margin: const EdgeInsets.only(left: 5),
                              child: Text(
                                "Resend",
                                style: text12(primaryRed, regular),
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: Get.height * 0.05,
                      ),
                      RoundedButton(
                        text: "Konfirmasi",
                        onTap: () {
                          if (controller.formkey.currentState!.validate()) {
                            controller.formkey.currentState!.save();
                            if (controller.pin1.value != "" &&
                                controller.pin2.value != "" &&
                                controller.pin3.value != "" &&
                                controller.pin4.value != "") {
                              controller.verifyOtp();
                            } else {
                              Get.snackbar(
                                  "Perhatian", "Kode OTP tidak lengkap");
                            }
                          }
                        },
                      )
                    ],
                  ))
            ],
          )),
    );
  }
}
