import 'package:flutter/material.dart';

// const Color primaryRed = Color(0xFF920F1D);
const Color primaryRed = Color(0xFF0A3680);
const Color primaryBlue = Color(0xFF032B44);
Color secondaryGrey = const Color(0xFF000000).withOpacity(0.05);

const Color black = Color(0xFF000000);
const Color white = Color(0xFFFFFFFF);
