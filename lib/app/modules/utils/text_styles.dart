import 'package:flutter/material.dart';

const FontWeight light = FontWeight.w300;
const FontWeight regular = FontWeight.w400;
const FontWeight medium = FontWeight.w500;
const FontWeight semibold = FontWeight.w600;
const FontWeight bold = FontWeight.w700;

TextStyle text36(colorText, fontWeight) {
  return TextStyle(
    color: colorText,
    fontSize: 36,
    fontWeight: fontWeight,
  );
}

TextStyle text24(colorText, fontWeight) {
  return TextStyle(
    color: colorText,
    fontSize: 24,
    fontWeight: fontWeight,
  );
}

TextStyle text22(colorText, fontWeight) {
  return TextStyle(
    color: colorText,
    fontSize: 22,
    fontWeight: fontWeight,
  );
}

TextStyle text20(colorText, fontWeight) {
  return TextStyle(
    color: colorText,
    fontSize: 20,
    fontWeight: fontWeight,
  );
}

TextStyle text18(colorText, fontWeight) {
  return TextStyle(
    color: colorText,
    fontSize: 18,
    fontWeight: fontWeight,
  );
}

TextStyle text16(colorText, fontWeight) {
  return TextStyle(
    color: colorText,
    fontSize: 16,
    fontWeight: fontWeight,
  );
}

TextStyle text14(colorText, fontWeight) {
  return TextStyle(
    color: colorText,
    fontSize: 14,
    fontWeight: fontWeight,
  );
}

TextStyle text12(colorText, fontWeight) {
  return TextStyle(
    color: colorText,
    fontSize: 12,
    fontWeight: fontWeight,
  );
}
