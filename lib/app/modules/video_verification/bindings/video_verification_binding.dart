import 'package:get/get.dart';

import '../controllers/video_verification_controller.dart';

class VideoVerificationBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<VideoVerificationController>(
      () => VideoVerificationController(),
    );
  }
}
