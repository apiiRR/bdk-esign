import 'dart:convert';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:video_player/video_player.dart';

import '../../../data/model/response_model.dart';
import '../../../data/services/generate_certificate/generate_certificate_service.dart';
import '../../../routes/app_pages.dart';
import '../../utils/colors.dart';
import '../../utils/text_styles.dart';

class VideoVerificationController extends GetxController {
  late List<CameraDescription> cameras;
  late CameraController cameraController;
  late VideoPlayerController videoPlayerController;
  late ChewieController chewieController;

  var isCameraOpen = false.obs;
  var startRecord = false.obs;
  var isLoading = false.obs;
  var isError = false.obs;
  var errmsg = "".obs;
  ResponseModel? data;

  final String jwt = Get.arguments["jwt"];
  final String email = Get.arguments["email"];

  XFile? fileVideo;
  late Widget liveIntruc;
  final List<String> listInstruction = [
    "Mohon kedipkan mata anda",
    "Mohon sekali lagi kedipkan mata anda",
    "Mohon buka mulut anda",
    "Mohon tutup kembali mulut anda"
  ];

  void openCamera() {
    isCameraOpen(true);
  }

  @override
  void onInit() {
    startCamera();
    super.onInit();
  }

  void startVideo() async {
    videoPlayerController = VideoPlayerController.file(File(fileVideo!.path));
    await videoPlayerController.initialize();
    chewieController = ChewieController(
      videoPlayerController: videoPlayerController,
      autoPlay: true,
      looping: true,
    );
    update();
  }

  void startCamera() async {
    cameras = await availableCameras();

    cameraController = CameraController(
      cameras[1],
      ResolutionPreset.medium,
      enableAudio: false,
    );

    await cameraController.initialize().then((value) {
      if (Get.isRegistered<CameraController>()) {
        return;
      }

      update();
    }).catchError((e) {
      print(e);
    });
  }

  @override
  void onClose() {
    cameraController.dispose();
    videoPlayerController.dispose();
    chewieController.dispose();
    super.onClose();
  }

  Future<void> instruction() async {
    for (var element in listInstruction) {
      liveIntruc = Text(
        element,
        style: text14(primaryRed, regular),
      );
      update();
      await Future.delayed(const Duration(seconds: 2));
    }
  }

  Future<void> record() async {
    startRecord(true);
    cameraController.startVideoRecording();
    await instruction();
    XFile recording = await cameraController.stopVideoRecording();
    fileVideo = recording;
    startRecord(false);
    update();
  }

  Future sendVideo() async {
    print(jwt);
    isLoading(true);
    if (fileVideo == null) {
      isLoading(false);
      Get.snackbar("Kesalahan", "Video tidak tersedia");
      return;
    }

    File videoFile = File(fileVideo!.path);
    Uint8List videoBytes = await videoFile.readAsBytes();
    String base64String = base64.encode(videoBytes);
    // print(videoFile.lengthSync() / (1024 * 1024));
    // print(base64String);

    try {
      final sendVideo = await GenerateCertificateService()
          .postVideoVerification(email, base64String, jwt);

      sendVideo.fold((l) {
        data = l;
      }, (r) {
        errmsg.value = r;
        isError(true);
      });

      isLoading(false);

      if (data == null) {
        isLoading(false);
        Get.snackbar("Kesalahan", errmsg.value);
        return;
      }

      print("KODE KERAS ${data!.resultCode}");

      switch (data!.resultCode) {
        case "0":
          await Get.defaultDialog(
            title: "Berhasil",
            titleStyle: text14(black, medium),
            content: Text(
              "Sertifikat elektronik anda dalam proses penerbitan, mohon untuk cek sertifikat anda",
              style: text12(black, regular),
            ),
            confirm: ElevatedButton(
              onPressed: () {
                Get.offAllNamed(Routes.INTRO);
              },
              style: ElevatedButton.styleFrom(backgroundColor: primaryRed),
              child: Text(
                "Selesai",
                style: text12(white, regular),
              ),
            ),
          );

          break;
        case "1006":
          Get.defaultDialog(
            title: "Warning",
            titleStyle: text14(black, medium),
            content: Text(
              data!.resultDesc,
              style: text12(black, regular),
              textAlign: TextAlign.center,
            ),
            confirm: ElevatedButton(
              onPressed: () {
                Get.back();
              },
              style: ElevatedButton.styleFrom(backgroundColor: primaryRed),
              child: Text(
                "Ulangi",
                style: text12(white, regular),
              ),
            ),
          );
          break;
        case "1040":
          await Get.defaultDialog(
            title: "Berhasil",
            titleStyle: text14(black, medium),
            content: Text(
              "Sertifikat elektronik anda dalam proses penerbitan, mohon untuk cek sertifikat anda",
              style: text12(black, regular),
              textAlign: TextAlign.center,
            ),
            confirm: ElevatedButton(
              onPressed: () {
                Get.offAllNamed(Routes.INTRO);
              },
              style: ElevatedButton.styleFrom(backgroundColor: primaryRed),
              child: Text(
                "Selesai",
                style: text12(white, regular),
              ),
            ),
          );
          break;
        default:
      }
      // Get.defaultDialog(
      //     title: "Error",
      //     titleStyle: text14(black, medium),
      //     content: Text(
      //       "${sendVideo["resultCode"]} | ${sendVideo["resultDesc"]}",
      //       style: text12(black, regular),
      //     ));
    } catch (error) {
      isLoading(false);
      isError(true);
      errmsg(error.toString());
      throw Exception("Error $error");
    }
  }
}
