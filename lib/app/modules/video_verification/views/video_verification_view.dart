import 'package:camera/camera.dart';
import 'package:chewie/chewie.dart';
import 'package:e_sign/app/modules/widgets/rounded_button.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../utils/colors.dart';
import '../../utils/text_styles.dart';
import '../../widgets/loading_button.dart';
import '../controllers/video_verification_controller.dart';

class VideoVerificationView extends GetView<VideoVerificationController> {
  const VideoVerificationView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: primaryRed,
          title: Text(
            'Verifikasi Wajah',
            style: text16(white, medium),
          ),
          centerTitle: true,
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: Get.width * 0.05),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(
                  height: 10,
                ),
                Text(
                  "Kami melakukan perekaman terhadap wajah anda untuk dilakukan verifikasi data biometrik dengan proses analisa dan perbandingan data referensi. Mohon untuk tidak mengenakan aksesoris di wajah ketika melakukan perekaman (misal: Kaca mata).",
                  style: text12(black, regular),
                ),
                const SizedBox(
                  height: 10,
                ),
                Obx(() => controller.isCameraOpen.isFalse
                    ? RoundedButton(
                        text: "Aktifkan Kamera",
                        onTap: () {
                          controller.openCamera();
                        })
                    : Container(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        width: Get.width,
                        // height: 500,
                        decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.5),
                            borderRadius: BorderRadius.circular(8)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Obx(() => controller.startRecord.isFalse
                                ? Column(
                                    children: [
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      RoundedButton(
                                          text: controller.fileVideo != null
                                              ? "Rekam Ulang"
                                              : "Mulai Rekam",
                                          onTap: () async {
                                            if (controller.fileVideo != null) {
                                              controller.fileVideo = null;
                                              controller.update();
                                            } else {
                                              await controller.record();
                                              controller.startVideo();
                                            }
                                          }),
                                    ],
                                  )
                                : const SizedBox()),
                            const SizedBox(
                              height: 10,
                            ),
                            GetBuilder<VideoVerificationController>(
                              init: VideoVerificationController(),
                              builder: (_) => controller.fileVideo != null
                                  ? Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Preview : ",
                                          style: text14(black, medium),
                                        ),
                                        const SizedBox(
                                          height: 2,
                                        ),
                                        SizedBox(
                                          width: Get.width,
                                          height: 300,
                                          child: controller
                                                  .videoPlayerController
                                                  .value
                                                  .isInitialized
                                              // ? VideoPlayer(controller
                                              //     .videoPlayerController)
                                              ? Chewie(
                                                  controller: controller
                                                      .chewieController)
                                              : const SizedBox(),
                                        )
                                      ],
                                    )
                                  : Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Ikuti Instruksi Berikut : ",
                                          style: text14(black, medium),
                                        ),
                                        const SizedBox(
                                          height: 2,
                                        ),
                                        GetBuilder<VideoVerificationController>(
                                            builder: (controller) =>
                                                controller.startRecord.isTrue
                                                    ? Column(
                                                        children: [
                                                          controller.liveIntruc,
                                                          const SizedBox(
                                                            height: 10,
                                                          ),
                                                        ],
                                                      )
                                                    : const SizedBox()),
                                        SizedBox(
                                            width: Get.width,
                                            height: Get.height /
                                                controller.cameraController
                                                    .value.aspectRatio,
                                            child: CameraPreview(
                                                controller.cameraController)),
                                      ],
                                    ),
                            ),
                            const SizedBox(
                              height: 10,
                            )
                          ],
                        ),
                      )),
                const SizedBox(
                  height: 10,
                ),
                Obx(() => controller.isLoading.isFalse
                    ? GetBuilder<VideoVerificationController>(
                        builder: (_) => controller.fileVideo != null
                            ? RoundedButton(
                                text: "Kirim",
                                onTap: () {
                                  controller.sendVideo();
                                })
                            : const SizedBox())
                    : const LoadingButton()),
              ],
            ),
          ),
        ));
  }
}
