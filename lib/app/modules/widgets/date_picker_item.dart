import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:intl/intl.dart';

import '../utils/colors.dart';
import '../utils/text_styles.dart';

class DatePickerItem extends StatelessWidget {
  const DatePickerItem(
      {super.key,
      required this.label,
      required this.name,
      required this.hintText,
      required this.validator,
      this.initialValue});

  final String label;
  final String name;
  final String hintText;
  final String? Function(DateTime?)? validator;
  final DateTime? initialValue;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
              label,
              style: text12(black, medium),
            ),
          ],
        ),
        const SizedBox(
          height: 6,
        ),
        Container(
          decoration: BoxDecoration(
              color: Colors.grey.withOpacity(0.3),
              borderRadius: BorderRadius.circular(8)),
          child: FormBuilderDateTimePicker(
            initialValue: initialValue,
            inputType: InputType.date,
            cursorColor: primaryRed,
            name: name,
            style: text12(black, regular),
            validator: validator,
            format: DateFormat("d/M/y"),
            decoration: InputDecoration(
              isDense: true,
              contentPadding: const EdgeInsets.all(10),
              hintText: hintText,
              hintStyle: text12(Colors.grey, regular),
              enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.transparent),
                  borderRadius: BorderRadius.circular(8)),
              focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: primaryRed, width: 2),
                  borderRadius: BorderRadius.circular(8)),
              border: OutlineInputBorder(
                  borderSide: const BorderSide(color: primaryRed, width: 2),
                  borderRadius: BorderRadius.circular(8)),
            ),
          ),
        )
      ],
    );
  }
}
