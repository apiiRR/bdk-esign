import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

import '../utils/colors.dart';
import '../utils/text_styles.dart';

class DropdownItem extends StatelessWidget {
  const DropdownItem(
      {super.key,
      required this.label,
      required this.name,
      required this.hintText,
      required this.validator,
      required this.items,
      this.info,
      this.maxLines = 1, this.initialValue});

  final String label;
  final String name;
  final String hintText;
  final int maxLines;
  final String? info;
  final String? Function(String?)? validator;
  final List<DropdownMenuItem<String>> items;
  final String? initialValue;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
              label,
              style: text12(black, medium),
            ),
            if (info != null) ...[
              Text(
                info!,
                style: const TextStyle(
                    color: primaryRed,
                    fontSize: 12,
                    fontWeight: regular,
                    fontStyle: FontStyle.italic),
              ),
            ]
          ],
        ),
        const SizedBox(
          height: 6,
        ),
        Container(
          decoration: BoxDecoration(
              color: Colors.grey.withOpacity(0.3),
              borderRadius: BorderRadius.circular(8)),
          child: FormBuilderDropdown<String>(
            initialValue: initialValue,
            name: name,
            style: text12(black, regular),
            validator: validator,
            items: items,
            decoration: InputDecoration(
                isDense: true,
                contentPadding: const EdgeInsets.all(10),
                hintText: hintText,
                hintStyle: text12(Colors.grey, regular),
                enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.transparent),
                    borderRadius: BorderRadius.circular(8)),
                focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: primaryRed, width: 2),
                    borderRadius: BorderRadius.circular(8)),
                border: OutlineInputBorder(
                    borderSide: const BorderSide(color: primaryRed, width: 2),
                    borderRadius: BorderRadius.circular(8))),
          ),
        )
      ],
    );
  }
}
