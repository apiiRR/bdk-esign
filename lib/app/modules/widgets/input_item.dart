import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

import '../utils/colors.dart';
import '../utils/text_styles.dart';

class InputItem extends StatefulWidget {
  const InputItem(
      {super.key,
      required this.label,
      required this.name,
      required this.hintText,
      required this.validator,
      required this.keyboardType,
      this.isPassword = false,
      this.info,
      this.maxLines = 1,
      this.initialValue,
      this.onChanged});

  final String label;
  final String name;
  final String hintText;
  final int maxLines;
  final String? info;
  final String? Function(String?)? validator;
  final bool isPassword;
  final TextInputType? keyboardType;
  final String? initialValue;
  final Function(String?)? onChanged;

  @override
  State<InputItem> createState() => _InputItemState();
}

class _InputItemState extends State<InputItem> {
  bool isVisible = true;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
              widget.label,
              style: text12(black, medium),
            ),
            if (widget.info != null) ...[
              Text(
                widget.info!,
                style: const TextStyle(
                    color: primaryRed,
                    fontSize: 12,
                    fontWeight: regular,
                    fontStyle: FontStyle.italic),
              ),
            ]
          ],
        ),
        const SizedBox(
          height: 6,
        ),
        Container(
          decoration: BoxDecoration(
              color: Colors.grey.withOpacity(0.3),
              borderRadius: BorderRadius.circular(8)),
          child: FormBuilderTextField(
            onChanged: widget.onChanged,
            initialValue: widget.initialValue,
            keyboardType: widget.keyboardType,
            obscureText: widget.isPassword == true ? isVisible : false,
            cursorColor: primaryRed,
            name: widget.name,
            maxLines: widget.maxLines,
            style: text12(black, regular),
            validator: widget.validator,
            decoration: InputDecoration(
                isDense: true,
                contentPadding: const EdgeInsets.all(10),
                hintText: widget.hintText,
                hintStyle: text12(Colors.grey, regular),
                enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.transparent),
                    borderRadius: BorderRadius.circular(8)),
                focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: primaryRed, width: 2),
                    borderRadius: BorderRadius.circular(8)),
                border: OutlineInputBorder(
                    borderSide: const BorderSide(color: primaryRed, width: 2),
                    borderRadius: BorderRadius.circular(8)),
                suffix: widget.isPassword == true
                    ? InkWell(
                        onTap: () {
                          setState(() {
                            isVisible = !isVisible;
                          });
                        },
                        child: const Icon(
                          Icons.remove_red_eye_outlined,
                          size: 16,
                        ))
                    : null),
          ),
        )
      ],
    );
  }
}
