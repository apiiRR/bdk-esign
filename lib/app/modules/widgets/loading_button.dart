import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../utils/colors.dart';

class LoadingButton extends StatelessWidget {
  const LoadingButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
          color: primaryRed, borderRadius: BorderRadius.circular(8)),
      child: const Center(
        child: CircularProgressIndicator(color: white),
      ),
    );
  }
}