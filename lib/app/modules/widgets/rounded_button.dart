import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../utils/colors.dart';
import '../utils/text_styles.dart';

class RoundedButton extends StatelessWidget {
  const RoundedButton({super.key, required this.text, required this.onTap});

  final String text;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: onTap,
        style: ElevatedButton.styleFrom(
            minimumSize: Size(Get.width, 42),
            backgroundColor: primaryRed,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12))),
        child: Text(
          text,
          style: text14(white, bold),
        ));
    // return InkWell(
    //   onTap: onTap,
    //   child: Container(
    //     width: Get.width,
    //     padding: const EdgeInsets.all(14),
    //     decoration: BoxDecoration(
    //         color: primaryRed, borderRadius: BorderRadius.circular(8)),
    //     child: Center(
    //       child: Text(
    //         text,
    //         style: text16(white, semibold),
    //       ),
    //     ),
    //   ),
    // );
  }
}
