import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../utils/colors.dart';
import '../utils/text_styles.dart';

class RoundedButtonUnfill extends StatelessWidget {
  const RoundedButtonUnfill(
      {super.key, required this.text, required this.onTap});

  final String text;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: onTap,
        style: ElevatedButton.styleFrom(
            minimumSize: Size(Get.width, 42),
            backgroundColor: white,
            shape: RoundedRectangleBorder(
                side: const BorderSide(color: primaryRed),
                borderRadius: BorderRadius.circular(12))),
        child: Text(
          text,
          style: text14(primaryRed, bold),
        ));
  }
}
