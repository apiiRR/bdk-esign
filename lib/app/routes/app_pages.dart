import 'package:get/get.dart';

import '../modules/cek_sertifikat/bindings/cek_sertifikat_binding.dart';
import '../modules/cek_sertifikat/views/cek_sertifikat_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/intro/bindings/intro_binding.dart';
import '../modules/intro/views/intro_view.dart';
import '../modules/otp/bindings/otp_binding.dart';
import '../modules/otp/views/otp_view.dart';
import '../modules/video_verification/bindings/video_verification_binding.dart';
import '../modules/video_verification/views/video_verification_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.INTRO;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => const HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.INTRO,
      page: () => const IntroView(),
      binding: IntroBinding(),
    ),
    GetPage(
      name: _Paths.CEK_SERTIFIKAT,
      page: () => CekSertifikatView(),
      binding: CekSertifikatBinding(),
    ),
    GetPage(
      name: _Paths.VIDEO_VERIFICATION,
      page: () => const VideoVerificationView(),
      binding: VideoVerificationBinding(),
    ),
    GetPage(
      name: _Paths.OTP,
      page: () => const OtpView(),
      binding: OtpBinding(),
    ),
  ];
}
