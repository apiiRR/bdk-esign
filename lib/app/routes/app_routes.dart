part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const INTRO = _Paths.INTRO;
  static const CEK_SERTIFIKAT = _Paths.CEK_SERTIFIKAT;
  static const VIDEO_VERIFICATION = _Paths.VIDEO_VERIFICATION;
  static const OTP = _Paths.OTP;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const INTRO = '/intro';
  static const CEK_SERTIFIKAT = '/cek-sertifikat';
  static const VIDEO_VERIFICATION = '/video-verification';
  static const OTP = '/otp';
}
